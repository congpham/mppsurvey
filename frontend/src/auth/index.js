const authKey = 'auth';
const login = (auth) => {
  localStorage.setItem(authKey, JSON.stringify(auth));
}
const getAuth = () => {
  const t = localStorage.getItem(authKey);
  if (!t) return { name: 'anonymous', isAuth: false, isAdmin: false };
  return JSON.parse(t);
}
const logout = () => {
  localStorage.setItem(authKey, '');
}
export default { login, logout, getAuth };