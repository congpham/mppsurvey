/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '/dashboard',
    // Relative to /src/views
    view: 'Dashboard',
    meta: { admin: true }
  },
  {
    path: '/user-profile',
    name: 'User Profile',
    view: 'UserProfile'
  },
  {
    path: '/surveys-list',
    name: 'Surveys',
    view: 'SurveysList',
    meta: { admin: true }
  },
  {
    path: '/login',
    name: 'Login',
    view: 'Login'
  },
  {
    path: '/home',
    name: 'Home',
    view: 'Home'
  },
  {
    path: '/survey',
    name: 'Survey',
    view: 'Survey'
  },
  {
    path: '/questions',
    name: 'Questions',
    view: 'Questions',
    meta: { admin: true }
  },
  {
    path: '/result',
    name: 'Result',
    view: 'Result',
    meta: { admin: true }
  },
  {
    path: '/category',
    name: 'Category List',
    view: 'CategoryList',
    meta: { admin: true }
  }
]
