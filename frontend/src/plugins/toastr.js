import Vue from 'vue'
import Toasted from 'vue-toasted'

Vue.use(Toasted,{
  position: 'top-right',
  duration: 4000,
  icon: 'info_outline',//'error_outline' || 'info_outline' || 'notifications_none'
  action : {
    icon : 'close',
    onClick : (e, toastObject) => {
        console.log('toastObject',toastObject.el.classList.contains('default'));
        console.log('e',e);
        toastObject.goAway(0);
    }
},
})
