import Vue from 'vue'
import auth from '../auth';
// Lib imports
import axios from 'axios'
axios.interceptors.request.use(
  (config) => {
    let token = auth.getAuth().token;
    if (token) config.headers['Authorization'] = `Bearer ${token}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
Vue.prototype.$http = axios
