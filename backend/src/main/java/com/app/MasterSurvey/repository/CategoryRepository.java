package com.app.MasterSurvey.repository;

import com.app.MasterSurvey.domain.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CategoryRepository extends MongoRepository<Category, Integer> {
    Category findById(String id);
    Category findByName(String name);
}
