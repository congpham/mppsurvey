package com.app.MasterSurvey.repository;

import com.app.MasterSurvey.domain.Result;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ResultRepository extends MongoRepository<Result, Integer> {
    List<Result> findBySurveyId(String surveyId);
}
