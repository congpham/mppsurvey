package com.app.MasterSurvey.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.MasterSurvey.domain.Admin;

public interface AdminRepository extends MongoRepository<Admin, Integer> {
    Admin findById(String id);

    Admin findByUsernameAndPassword(String username, String password);

    Admin findByToken(String token);
}
