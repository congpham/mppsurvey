package com.app.MasterSurvey.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.app.MasterSurvey.domain.Survey;


import java.util.List;

public interface SurveyRepository extends MongoRepository<Survey, Integer> {
    Survey findById(String id);

    List<Survey> findByActive(Boolean active);

    List<Survey> findByIsActive(Boolean active);
}
