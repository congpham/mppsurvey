package com.app.MasterSurvey.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@CrossOrigin
public class RequestFilter extends GenericFilterBean {

    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;

        if ("OPTIONS".equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
            chain.doFilter(req, res);
            return;
        }

        String authHeader = request.getHeader("authorization");
        String token;
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            authHeader = request.getHeader("token");
            if (authHeader == null) {
                throw new ServletException("Missing or invalid Authorization header");
            }
            token = authHeader;
        } else {
            token = authHeader.substring(7);
        }

        //bypass token validation for testing
        System.out.println("Token is: " + token);
        if (token.equals("mpptoken")) {
            chain.doFilter(req, res);
            return;
        }

        try {
            final Claims claims = Jwts
                    .parser()
                    .setSigningKey("MaYzkSjmkzPC57L")
                    .parseClaimsJws(token).getBody();

            request.setAttribute("claims", claims);

            System.out.println(claims.getSubject());
            System.out.println(token);

            //lets validate this token with our database
            /*Admin admin = this.service.findByToken(token);

            if (admin == null) {
                throw new SignatureException("Token not found.");
            }

            //validate the expiry of token
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
            LocalDateTime then = format.parse(admin.getExpiresAt()).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            LocalDateTime now = LocalDateTime.now();

            Duration duration = Duration.between(now, then);
            long diff = duration.getSeconds();

            if (diff > 3 * 3600) {
                throw new ServletException("Token has expired");
            }*/
        } catch (final SignatureException e) {
            throw new ServletException("Invalid token");
        } /*catch (final ParseException e) {
            throw new ServletException(e.getMessage());
        } */ catch (final Exception e) {
            System.out.println(e.getMessage());
            throw new ServletException(e.getMessage());
        }

        chain.doFilter(req, res);
    }
}
