package com.app.MasterSurvey.library;

import com.app.MasterSurvey.domain.Question;
import com.app.MasterSurvey.domain.Survey;

public class Validation {
	public static void checkEmpty(Object object) throws Exception {
    	if(object == null) throw new Exception("You send an invalid request, please check your request param or body!");
    }
	public static void checkEmptyQuestionsSurvey(Survey survey) throws Exception {
    	if(survey.getQuestions().size() == 0) throw new Exception("Survey does not have any questions!");
    }
	public static Survey checkId(Survey survey) {
		for (Question question : survey.getQuestions()) {
            if (question.getId() != null) {
                continue;
            }
            question.makeId();
        }
		return survey;
	}
}
