package com.app.MasterSurvey.library;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class Hashing {

    @org.jetbrains.annotations.Contract(pure = true)
    public static String hash(String string) throws Exception {
        return Jwts
                .builder()
                .setSubject(string)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "MaYzkSjmkzPC57L")
                .compact();
    }
}
