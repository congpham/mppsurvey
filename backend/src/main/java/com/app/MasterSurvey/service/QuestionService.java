package com.app.MasterSurvey.service;

import com.app.MasterSurvey.domain.Question;
import com.app.MasterSurvey.domain.Survey;
import com.app.MasterSurvey.repository.SurveyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    private SurveyRepository surveyRepository;

    public QuestionService(@Autowired SurveyRepository surveyRepository) {
        this.surveyRepository = surveyRepository;
    }

    public Survey editQuestion(Question question, String surveyId) throws Exception {
        Survey survey = surveyRepository.findById(surveyId);
        //check if exists
        if (survey.getQuestions().stream().filter(q -> q.getId().equals(question.getId())).count() < 1) {
            throw new Exception("Question not found!");
        }
        if (!isCorrectType(question)) throw new Exception("The options and default option are not valid");
        survey.getQuestions().set(survey.getQuestions().indexOf(question), question);
        surveyRepository.save(survey);
        return survey;
    }

    public Survey deleteQuestion(Question question, String surveyId) throws Exception {
        Survey survey = surveyRepository.findById(surveyId);
        //check if exists
        if (survey.getQuestions().stream().filter(q -> q.getId().equals(question.getId())).count() < 1) {
            throw new Exception("Question not found!");
        }
        survey.getQuestions().remove(question);
        surveyRepository.save(survey);
        return survey;
    }

    public static boolean isExisted(Survey survey, Question question) {

        //the question already exists
        for (Question q : survey.getQuestions()) {
            if (q.getContent().equals(question.getContent())) {
                return true;
            }
        }
        return false;
    }

    private boolean isCorrectType(Question question) {
        if (question == null) return false;
        //for mc question, the options and default option must be not null
        if (question.getType().toLowerCase().equals("mc")) {
            String[] options = question.getOptions();
            if (options.length != 4) return false;
        }
        return true;
    }

    public static void toLowerCase(Question question) {
        if (question == null) return;
        //lower case type
        String type = question.getType();
        if (type == null) type = "";
        question.setType(type.toLowerCase());
    }

    public List<Question> getSurveyQuestions(String id) {
        Survey survey = this.surveyRepository.findById(id);
        return survey.getQuestions();
    }

    public Question getSurveyQuestion(String surveyId, String questionId) {
        List<Question> questions = this.getSurveyQuestions(surveyId);
        return questions
                .stream()
                .reduce(null, (Question question, Question question2) -> question = question2.getId().equals(questionId) ? question2 : question);
    }

    public Question addSurveyQuestion(String surveyId, Question question) throws Exception {
        Survey survey = this.surveyRepository.findById(surveyId);
        //check empty
        if (survey == null || question == null) {
            throw new Exception("Survey is not found!");
        }
        if (isExisted(survey, question)) {
            throw new Exception("Question already exists, please using edit instead");
        }
        if (!isCorrectType(question)) throw new Exception("The options and default option are not valid");
        toLowerCase(question);
        question.makeId();
        survey.addQuestion(question);
        this.surveyRepository.save(survey);
        return question;
    }

    public Survey addSurveyQuestions(String surveyId, List<Question> questions) {
        Survey survey = this.surveyRepository.findById(surveyId);
        List<Question> save = questions
                .stream()
                .filter(question -> !isExisted(survey, question) && isCorrectType(question))
                .collect(Collectors.toList());

        for (Question question : save) {
            toLowerCase(question);
            question.makeId();
            survey.addQuestion(question);
        }
        return this.surveyRepository.save(survey);
    }
}
