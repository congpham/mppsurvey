package com.app.MasterSurvey.service;

import java.util.List;
import java.util.stream.Collectors;

import com.app.MasterSurvey.domain.Survey;
import com.app.MasterSurvey.library.Validation;
import com.app.MasterSurvey.repository.SurveyRepository;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SurveyService {
    private SurveyRepository surveyRepository;

    public SurveyService(@Autowired SurveyRepository surveyRepository) {
        this.surveyRepository = surveyRepository;
    }

    @Contract("_ -> param1")
    public static Survey makeActive(@NotNull Survey survey) {
        survey.setActive(true);
        return survey;
    }

    public Survey findById(@NotNull String id) {
        return this.surveyRepository.findById(id);
    }

    public Survey postSurvey(Survey survey) throws Exception {
        Validation.checkEmpty(survey);
        Validation.checkId(survey);
        makeActive(survey);
        return this.surveyRepository.save(survey);
    }

    public Survey editSurvey(Survey survey) throws Exception {
        Validation.checkEmpty(survey);
        Validation.checkId(survey);
        return this.surveyRepository.save(survey);
    }

    public List<Survey> getSurveys() {
        return this.surveyRepository.findAll();
    }

    public List<Survey> postSurveys(List<Survey> surveys) throws Exception {
        Validation.checkEmpty(surveys);
        int size = surveys.size();
        for (int index = 0; index < size; index++) {
            Survey survey = surveys.get(index);
            Validation.checkId(survey);
            makeActive(survey);
            surveys.set(index, survey);
        }
        return this.surveyRepository.saveAll(surveys);
    }

    public List<Survey> getSurveysByCategory(String cat) throws Exception {
        List<Survey> surveys = this.surveyRepository.findAll();
        if (cat == null || cat.equals("")) {
            return surveys;
        }
        return surveys.stream().filter(s -> s.getCategory().equals(cat)).collect(Collectors.toList());
    }

    public List<Survey> searchSurveysByCategory(String cat) throws Exception {
        List<Survey> surveys = this.surveyRepository.findAll();
        if (cat == null || cat.equals("")) {
            return surveys;
        }
        return surveys.stream().filter(s -> s.getCategory().toLowerCase().contains(cat.toLowerCase())).collect(Collectors.toList());
    }

    public void deleteSurvey(Survey survey) throws Exception {
        this.surveyRepository.delete(survey);
    }

    public Survey deactivateSurvey(String id) throws Exception {
        Survey survey = findById(id);
        Validation.checkEmpty(survey);
        survey.setActive(false);
        return this.surveyRepository.save(survey);
    }

    public Survey activateSurvey(String id) throws Exception {
        Survey survey = findById(id);
        Validation.checkEmpty(survey);
        survey.setActive(true);
        return this.surveyRepository.save(survey);
    }

    public List<Survey> getActivatedSurveys() {
        return this.surveyRepository.findByIsActive(true);
    }
}
