package com.app.MasterSurvey.service;

import com.app.MasterSurvey.domain.Category;
import com.app.MasterSurvey.repository.CategoryRepository;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    private CategoryRepository categoryRepository;
    private MongoTemplate template;

    public CategoryService(@Autowired CategoryRepository categoryRepository, MongoTemplate template) {
        this.categoryRepository = categoryRepository;
        this.template = template;
    }

    public List<Category> getAll() {
        return this.categoryRepository.findAll();
    }

    public Category findById(String id) {
        return this.categoryRepository.findById(id);
    }

    private Category findByName(String name) {
        return this.categoryRepository.findByName(name);
    }

    private Category saveCategory(Category category) {
        return this.categoryRepository.save(category);
    }

    public Category addCategory(Category category) throws Exception {
        if (category.getName().length() < 2) {
            throw new Exception("The name of category should contain at least 2 characters.");
        }

        Category cat = this.findByName(category.getName());
        if (cat != null && cat.getName().equals(category.getName())) {
            throw new Exception("A category with the same name already exists.");
        }

        return this.saveCategory(category);
    }

    public Category updateCategory(Category category) throws Exception {
        if (category.getId() == null) {
            throw new Exception("Category does not have a unique id.");
        }

        Category cat = this.findById(category.getId());
        if (cat == null) {
            throw new Exception("The category your are trying to modify was not found.");
        }

        Category catByName = this.findByName(category.getName());

        if (catByName != null &&
                catByName.getName().equals(category.getName()) &&
                !catByName.getId().equals(cat.getId())
        ) {
            throw new Exception("A category with the same name already exists.");
        }

        return this.saveCategory(category);
    }

    public Category deleteCategoryById(String id) throws Exception {
        Category category = this.categoryRepository.findById(id);
        if (category == null) {
            throw new Exception("Category to be deleted was not found.");
        }

        DeleteResult result = this.template.remove(Query.query(Criteria.where("id").is(id)), Category.class, "survey_categories");
        if (result.getDeletedCount() == 0) {
            throw new Exception("Category could not be deleted.");
        }

        return category;
    }
}
