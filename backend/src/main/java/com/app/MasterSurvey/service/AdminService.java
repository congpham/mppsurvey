package com.app.MasterSurvey.service;

import com.app.MasterSurvey.domain.Admin;
import com.app.MasterSurvey.library.Hashing;

import com.app.MasterSurvey.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class AdminService {
    private AdminRepository adminRepository;

    public AdminService(@Autowired AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public Admin findById(String id) {
        return this.adminRepository.findById(id);
    }

    public Admin findByUsernameAndPassword(String username, String password) {
        return this.adminRepository.findByUsernameAndPassword(username, password);
    }

    public Admin findByToken(String token) {
        return this.adminRepository.findByToken(token);
    }

    public String getAuthToken(Admin admin) throws Exception {
        try {
            String hash = admin.getUsername() + "-" + admin.getPassword();
            return Hashing.hash(hash);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception("We are unable to generate a token for you at the moment.");
        }
    }

    public Admin saveAuthToken(Admin admin) {
        LocalDateTime dateTime = LocalDateTime.now();
        LocalDateTime expires = dateTime.plusSeconds(3600 * 3);
        String date = expires.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        admin.setExpiresAt(date);
        return this.adminRepository.save(admin);
    }

    public Admin createAdmin(Admin admin) {
        return this.adminRepository.save(admin);
    }
}
