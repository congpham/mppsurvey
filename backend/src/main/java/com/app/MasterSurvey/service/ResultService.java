package com.app.MasterSurvey.service;

import com.app.MasterSurvey.View.Model.AnswerChart;
import com.app.MasterSurvey.View.Model.QuestionChart;
import com.app.MasterSurvey.View.Model.SurveyChart;
import com.app.MasterSurvey.domain.Question;
import com.app.MasterSurvey.domain.Result;
import com.app.MasterSurvey.domain.Survey;
import com.app.MasterSurvey.library.Validation;
import com.app.MasterSurvey.repository.ResultRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ResultService {
    private ResultRepository repository;
    @Autowired
    private SurveyService surveyService;
    
    public ResultService(@Autowired ResultRepository repository) {
        this.repository = repository;
    }

    public Result addResult(Result result) throws Exception {
    	if(!isValid(result)) throw new Exception("Result is not valid.");
        return this.repository.save(result);
    }

    public List<Result> addResults(List<Result> results) throws Exception {
    	List<Result> rs = results.stream().filter(r -> isValid(r)).collect(Collectors.toList());
    	if (rs == null || results.size() != rs.size()) throw new Exception("Result is not valid.");
        return this.repository.saveAll(results);
    }

    public List<Result> fetchAll(String surveyId) {
        return this.repository.findBySurveyId(surveyId);
    }
    
    public List<Result> fetchAllByQuestionId(String surveyId, String questionId) {
        return this.repository.findBySurveyId(surveyId).stream().filter(r->r.getQuestionId().equals(questionId)).collect(Collectors.toList());
    }
    
    public List<Result> fetchAll() {
        return this.repository.findAll();
    }
    
    public SurveyChart fetchSurveyChart(String surveyId) throws Exception{
    	Survey survey = surveyService.findById(surveyId);
    	Validation.checkEmpty(survey);
    	return initSurveyChartViewModel(survey);
    }
    
    private SurveyChart initSurveyChartViewModel(Survey survey) {
    	String surveyId = survey.getId();
    	List<Question> questions = survey.getQuestions();
    	SurveyChart surveyChart = new SurveyChart();
    	List<QuestionChart> qc = new ArrayList<QuestionChart>();
    	
    	for (Question q : questions) {
    		List<Result> results = fetchAllByQuestionId(surveyId, q.getId());
    		String[] options = q.getOptions();
    		QuestionChart questionChart = new QuestionChart();
    		questionChart.setContent(q.getContent());
    		questionChart.setType(q.getType());
    		String[] answers = results.stream().map(Result::getAnswer).toArray(String[]::new);
    		if (q.getType().toLowerCase().equals("mc")) {
    			AnswerChart[] aws = calculateAnswerChart(options, answers);
    			questionChart.setOptions(aws);
    		}
    		else if (q.getType().toLowerCase().equals("oe")) {
    			questionChart.setAnswers(answers);
    		}
    		questionChart.setRating(calculateRating(results.stream().map(Result::getRating).toArray(Double[]::new)));
    		qc.add(questionChart);
    	}
    	surveyChart.setQuestions(qc);
    	surveyChart.setSurveyId(surveyId);
    	surveyChart.setName(survey.getName());
    	return surveyChart;
    }
    
    private AnswerChart[] calculateAnswerChart(String[] options, String[] answers) {
    	Map<String, Double> percentage = calculatePercentage(options, answers);
    	List<AnswerChart> aws = new ArrayList<AnswerChart>();
    	for (Map.Entry<String, Double> val : percentage.entrySet()) {
    		AnswerChart answerChart = new AnswerChart();
    		answerChart.setContent(val.getKey());
    		answerChart.setPercent(val.getValue().doubleValue());
    		aws.add(answerChart);
    	}
    	return aws.stream().toArray(AnswerChart[]::new);
    }
    
    private static Map<String, Double> countFrequencies(String[] options, String[] list) {
    	Map<String, Double> map = new HashMap<String, Double>();
    	//initiate all the options to map
    	for(String key : options) {
    		map.put(key, 0.0);
    	}
    	//count the frequency
    	for(String key : list) {
    		Double count = map.get(key);
    		if(count == null) continue;
     		map.put(key, (count < 1) ? 1 : count + 1);		
    	}
    	return map;
    }
    
    private static Map<String, Double> calculatePercentage(String[] options, String[] list) {
    	Map<String, Double> frequency = countFrequencies(options, list);
    	Integer sum = list.length;
    	for (Map.Entry<String, Double> val : frequency.entrySet()) {
    		frequency.put(val.getKey(), val.getValue()/sum);
    	}
    	return frequency;
    }
    
    private boolean isValid(Result result) {
    	if (result == null) return false;
    	Survey survey = surveyService.findById(result.getSurveyId());
    	if(survey == null) return false;
    	Question question = survey.getQuestionById(result.getQuestionId());
    	if (question == null) return false;
    	//for oe question, the result content can be null
    	//for mc question, the result must be in the options
    	if(question.getType().toLowerCase().equals("mc")) {
    		String[] options = question.getOptions();
    		for (String opt : options) {
    			if (opt.equals(result.getAnswer())) return true;
    		}
    		return false;
    	}
    	return true;
    }
    
    private double calculateRating(Double[] ratings) {
    	double result = 0;
    	for(Double rating : ratings) {
    		result += rating.doubleValue();
    	}
    	return result/ratings.length;
    }
}
