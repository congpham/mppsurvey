package com.app.MasterSurvey.controller;

import com.app.MasterSurvey.domain.Category;
import com.app.MasterSurvey.service.CategoryService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin()
@RequestMapping("/api")
@RestController
public class CategoryController {
    private CategoryService categoryService;

    public CategoryController(CategoryService service) {
        this.categoryService = service;
    }

    @RequestMapping(path = "/category", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> getAllCategories() {
        return this.categoryService.getAll();
    }

    @RequestMapping(path = "/category", method = RequestMethod.POST)
    public @ResponseBody
    Category postAddCategory(@Valid @RequestBody Category category) throws Exception {
        if (category.getId() == null) {
            return this.categoryService.addCategory(category);
        }

        return this.categoryService.updateCategory(category);
    }

    @RequestMapping(path = "/category/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Category getCategory(@PathVariable("id") String category) {
        return this.categoryService.findById(category);
    }

    @RequestMapping(path = "/category/{id}/delete", method = RequestMethod.GET)
    public @ResponseBody
    Category getDeleteCategory(@PathVariable("id") String id) throws Exception {
        return this.categoryService.deleteCategoryById(id);
    }
}