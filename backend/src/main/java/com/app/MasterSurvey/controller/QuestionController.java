package com.app.MasterSurvey.controller;


import org.springframework.web.bind.annotation.*;

import com.app.MasterSurvey.domain.Question;
import com.app.MasterSurvey.domain.Survey;
import com.app.MasterSurvey.service.QuestionService;

import java.util.List;

@CrossOrigin()
@RestController()
@RequestMapping("api")
public class QuestionController {
    private QuestionService questionService;

    public QuestionController(QuestionService service) {
        this.questionService = service;
    }

    @RequestMapping(path = "/survey/{id}/question", method = RequestMethod.GET)
    public @ResponseBody
    List<Question> getSurveyQuestions(@PathVariable("id") String id) {
        return this.questionService.getSurveyQuestions(id);
    }

    @RequestMapping(path = "/survey/{surveyId}/question/{questionId}", method = RequestMethod.GET)
    public @ResponseBody
    Question getQuestion(@PathVariable("surveyId") String surveyId, @PathVariable("questionId") String questionId) {
        return this.questionService.getSurveyQuestion(surveyId, questionId);
    }

    @RequestMapping(path = "/survey/{surveyId}/questions", method = RequestMethod.POST)
    public @ResponseBody
    Survey addQuestions(@PathVariable("surveyId") String id, @RequestBody List<Question> questions) {
        return this.questionService.addSurveyQuestions(id, questions);
    }

    @RequestMapping(path = "/survey/{surveyId}/question", method = RequestMethod.POST)
    public @ResponseBody
    Question addQuestion(@PathVariable("surveyId") String id, @RequestBody Question question) throws Exception {
        return this.questionService.addSurveyQuestion(id, question);
    }

    @RequestMapping(path = "/survey/{surveyId}/question/edit", method = RequestMethod.POST)
    public @ResponseBody
    Survey editQuestion(@PathVariable("surveyId") String surveyId, @RequestBody Question question) throws Exception {
        return questionService.editQuestion(question, surveyId);
    }

    @RequestMapping(path = "/survey/{surveyId}/question/delete", method = RequestMethod.POST)
    public @ResponseBody
    Survey deleteQuestion(@PathVariable("surveyId") String surveyId, @RequestBody Question question) throws Exception{
        return questionService.deleteQuestion(question, surveyId);
    }

}
