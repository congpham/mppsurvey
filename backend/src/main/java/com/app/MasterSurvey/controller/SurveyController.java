package com.app.MasterSurvey.controller;

import org.springframework.web.bind.annotation.*;

import com.app.MasterSurvey.domain.Survey;
import com.app.MasterSurvey.service.SurveyService;

import java.util.List;

@CrossOrigin()
@RestController()
@RequestMapping("api")
public class SurveyController {
    private SurveyService surveyService;

    public SurveyController(SurveyService service) {
        this.surveyService = service;
    }

    @RequestMapping(path = "/surveys", method = RequestMethod.GET)
    public @ResponseBody
    List<Survey> getSurveys() {
        return surveyService.getSurveys();
    }

    @RequestMapping(path = "/surveys/add", method = RequestMethod.POST)
    public @ResponseBody
    List<Survey> postSurveysAdd(@RequestBody List<Survey> surveys) throws Exception {
        return surveyService.postSurveys(surveys);
    }

    @RequestMapping(path = "/surveys/{cat}")
    public @ResponseBody
    List<Survey> getSurveysByCategory(@PathVariable("cat") String cat) throws Exception {
        return surveyService.getSurveysByCategory(cat);
    }

    @RequestMapping(path = "/surveys/search/{cat}", method = RequestMethod.GET)
    public @ResponseBody
    List<Survey> searchSurveysByCategory(@PathVariable("cat") String cat) throws Exception {
        return surveyService.searchSurveysByCategory(cat);
    }

    @RequestMapping(path = "/survey", method = RequestMethod.POST)
    public @ResponseBody
    Survey postSurvey(@RequestBody Survey survey) throws Exception {
        return surveyService.postSurvey(survey);
    }

    @RequestMapping(path = "/survey/edit", method = RequestMethod.POST)
    public @ResponseBody
    Survey postEditSurvey(@RequestBody Survey survey) throws Exception {
        survey.setActive(true);
        return surveyService.editSurvey(survey);
    }

    @RequestMapping(path = "/survey/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Survey getSurvey(@PathVariable("id") String id) throws Exception {
        return surveyService.findById(id);
    }

    @RequestMapping(path = "/survey/delete", method = RequestMethod.POST)
    public @ResponseBody
    void deleteSurvey(@RequestBody Survey survey) throws Exception {
        surveyService.deleteSurvey(survey);
    }

    @RequestMapping(path = "/survey/deactivate/{id}")
    public @ResponseBody
    Survey deactivateSurvey(@PathVariable("id") String id) throws Exception {
        return surveyService.deactivateSurvey(id);
    }

    @RequestMapping(path = "/survey/activate/{id}")
    public @ResponseBody
    Survey activateSurvey(@PathVariable("id") String id) throws Exception {
        return surveyService.activateSurvey(id);
    }

    @RequestMapping(path = "/user/surveys", method = RequestMethod.GET)
    public @ResponseBody
    List<Survey> getUserSurveys() {
        return surveyService.getActivatedSurveys();
    }
}
