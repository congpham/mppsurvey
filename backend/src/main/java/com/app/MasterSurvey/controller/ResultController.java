package com.app.MasterSurvey.controller;

import com.app.MasterSurvey.View.Model.SurveyChart;
import com.app.MasterSurvey.domain.Result;
import com.app.MasterSurvey.service.ResultService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin()
@RestController()
@RequestMapping("api")
class ResultController {
    private ResultService service;

    ResultController(ResultService service) {
        this.service = service;
    }

    @RequestMapping(path = "/survey/results/submit", method = RequestMethod.POST)
    public @ResponseBody
    List<Result> submitSurvey(@RequestBody List<Result> results) throws Exception {
        return this.service.addResults(results);
    }

    @RequestMapping(path = "/survey/results/{id}", method = RequestMethod.GET)
    public @ResponseBody
    List<Result> fetchResults(@PathVariable("id") String surveyId) {
        return this.service.fetchAll(surveyId);
    }

    @RequestMapping(path = "/survey/results", method = RequestMethod.GET)
    public @ResponseBody
    List<Result> fetchResults() {
        return this.service.fetchAll();
    }
    
    @RequestMapping(path = "/survey/results/{sid}/{qid}", method = RequestMethod.GET)
    public @ResponseBody
    List<Result> fetchAllByQuestionId(@PathVariable("sid") String surveyId, @PathVariable("qid") String questionId) {
        return this.service.fetchAllByQuestionId(surveyId, questionId);
    }
    
    @RequestMapping(path = "/survey/results/chart/{id}", method = RequestMethod.GET)
    public @ResponseBody
    SurveyChart fetchSurveyChart(@PathVariable("id") String surveyId) throws Exception {
        return this.service.fetchSurveyChart(surveyId);
    }
}
