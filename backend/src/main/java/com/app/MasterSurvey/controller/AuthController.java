package com.app.MasterSurvey.controller;

import com.app.MasterSurvey.domain.Admin;
import com.app.MasterSurvey.service.AdminService;
import org.springframework.web.bind.annotation.*;

@CrossOrigin()
@RestController()
@RequestMapping("api")
public class AuthController {
    private AdminService adminService;

    AuthController(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public @ResponseBody
    Admin postAuthUser(@RequestBody Admin admin) throws Exception {
        Admin user = this.adminService.findByUsernameAndPassword(admin.getUsername(), admin.getPassword());

        if (user == null) {
            throw new Exception("User not found");
        }

        //lets update the token for this user
        user.setToken(this.adminService.getAuthToken(user));

        //not sure if we need this
        if (user.getToken() == null) {
            throw new Exception("We are unable to save your token at the moment.");
        }

        user = this.adminService.saveAuthToken(user);
        return user;
    }

    @RequestMapping(path = "/admin", method = RequestMethod.POST)
    public @ResponseBody
    Admin postCreateAdmin(@RequestBody Admin admin) throws Exception {
        return this.adminService.createAdmin(admin);
    }
}
