package com.app.MasterSurvey.View.Model;

import java.util.List;

public class SurveyChart {
	private String surveyId;
	private String name;
	private List<QuestionChart> questions;
	
	public String getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<QuestionChart> getQuestions() {
		return questions;
	}
	public void setQuestions(List<QuestionChart> questions) {
		this.questions = questions;
	}
	
	
}
