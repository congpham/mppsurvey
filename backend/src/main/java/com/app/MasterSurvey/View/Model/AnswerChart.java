package com.app.MasterSurvey.View.Model;

public class AnswerChart {
	private String content;
	private double percent;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public double getPercent() {
		return percent;
	}
	public void setPercent(double percent) {
		this.percent = percent;
	}
	
}
