package com.app.MasterSurvey.View.Model;

public class QuestionChart {
    private String content;
    private String type;
    private AnswerChart[] options;
    private String[] answers;
    private double rating;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AnswerChart[] getOptions() {
        return options;
    }

    public void setOptions(AnswerChart[] options) {
        this.options = options;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

}
