package com.app.MasterSurvey.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;


public class Question {
    @Id
    protected String id;
    protected String content;

    protected String type;

    protected String[] options = {};

    protected String defaultOption;

    public Question() {
    }

    public void setContent(String cont) {
        this.content = cont;
    }

    public String getContent() {
        return this.content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public String getDefaultOption() {
        return defaultOption;
    }

    public void setDefaultOption(String defaultOption) {
        this.defaultOption = defaultOption;
    }

    public void makeId() {
        if (this.id != null) {
            return;
        }

        this.id = (new ObjectId()).toString();
    }
    
    @Override
    public boolean equals(Object obj) {
    	// TODO Auto-generated method stub
    	Question question = (Question)obj;
    	return this.id.equals(question.getId());
    }
}
