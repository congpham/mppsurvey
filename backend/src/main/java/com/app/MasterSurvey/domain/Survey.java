package com.app.MasterSurvey.domain;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Survey {
    @Id
    protected String id;
    protected String name;
    protected String description;
    protected boolean isActive;
    protected String category;
    private List<Question> questions;

    public Survey() {
        this.questions = new ArrayList<Question>();
        isActive = true;
    }

    public Survey(String id, String name, String description, String category) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.isActive = true;
		this.category = category;
		this.questions = new ArrayList<Question>();
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Question getQuestionById(String id) {
        for (Question q : questions) {
            if (q.getId().equals(id)) {
                return q;
            }
        }
        return null;
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }
}
