package com.app.MasterSurvey.domain;

public class Rating {
	private Question question;
	private User user;
	private int rating;
	
	public Rating(Question q, User u, int a) {
		question = q;
		user = u;
		rating = a;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
}
