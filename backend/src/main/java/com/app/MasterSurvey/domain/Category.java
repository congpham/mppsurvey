package com.app.MasterSurvey.domain;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;

@Document(collection = "survey_categories")
public final class Category {
    @Id
    private String id;

    @NotNull
    @Size(min = 2, message = "Name of category should be at least 2 characters.", max = 32)
    private String name;

    private short status;

    public Category(@NotNull String name, short status) {
        this.name = name;
        this.status = status;
    }

    @Contract(pure = true)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Contract(pure = true)
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Contract(pure = true)
    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }
}
