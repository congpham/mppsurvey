package com.app.MasterSurvey.domain;

import java.util.List;

public class User {
    private String name;
    private List<Survey> surveys;

    public User() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }
}