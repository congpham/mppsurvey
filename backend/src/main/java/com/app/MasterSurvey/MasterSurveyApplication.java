package com.app.MasterSurvey;

import com.app.MasterSurvey.config.RequestFilter;
import com.app.MasterSurvey.service.AdminService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MasterSurveyApplication {

    @Bean
    public FilterRegistrationBean<RequestFilter> filterRegistrationBean() {
        final FilterRegistrationBean<RequestFilter> registrationBean = new FilterRegistrationBean<RequestFilter>();
        registrationBean.setFilter(new RequestFilter());
        registrationBean.addUrlPatterns("/api/surveys");
        registrationBean.addUrlPatterns("/api/surveys/add");
        registrationBean.addUrlPatterns("/api/surveys/*");
        registrationBean.addUrlPatterns("/api/surveys/search/*");

        registrationBean.addUrlPatterns("/api/survey");
        registrationBean.addUrlPatterns("/api/survey/edit");
        //registrationBean.addUrlPatterns("/api/survey/*");
        registrationBean.addUrlPatterns("/api/survey/delete");
        registrationBean.addUrlPatterns("/api/survey/deactivate/*");
        registrationBean.addUrlPatterns("/api/survey/activate/*");

        registrationBean.addUrlPatterns("/api/survey/*/question");
        registrationBean.addUrlPatterns("/api/survey/*/questions");
        registrationBean.addUrlPatterns("/api/survey/*/question/*");

        registrationBean.addUrlPatterns("/api/category");
        registrationBean.addUrlPatterns("/api/category/*");

        registrationBean.addUrlPatterns("/api/survey/results");
        registrationBean.addUrlPatterns("/api/survey/results/chart/*");
        registrationBean.addUrlPatterns("/api/survey/results/*/*");
        return registrationBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(MasterSurveyApplication.class, args);
    }
}
