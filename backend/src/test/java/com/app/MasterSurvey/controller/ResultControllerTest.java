package com.app.MasterSurvey.controller;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import io.restassured.http.Header;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Result;

import io.restassured.RestAssured;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MasterSurveyApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class ResultControllerTest {
    @LocalServerPort
    private int port;
    private List<Result> results = new ArrayList<Result>();

    public ResultControllerTest() {
        // TODO Auto-generated constructor stub
        Result r1 = new Result();
        r1.setSurveyId("1");
        r1.setQuestionId("1");
        r1.setAnswer("a");
        r1.setRating(3);

        results.add(r1);
    }

    @Before
    public void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Test
    public void controller_post_survey_success_if_survey_returned_in_body() {
        //send the request to the controller method

    }

    @Test
    public void getSurveys() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                when().
                get("/api/survey/results").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void testGetSurveyQuestion() {

    }
}
