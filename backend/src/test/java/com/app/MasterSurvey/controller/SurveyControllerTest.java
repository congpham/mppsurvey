package com.app.MasterSurvey.controller;

import com.app.MasterSurvey.service.SurveyService;
import io.restassured.http.Header;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Survey;

import io.restassured.RestAssured;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MasterSurveyApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class SurveyControllerTest {
    @LocalServerPort
    private int port;
    private Survey survey;

    @Autowired
    private SurveyService service;

    public SurveyControllerTest() {
        // TODO Auto-generated constructor stub
        this.survey = new Survey();
        survey.setId("5c832a8176af343ab85bc75c");
        survey.setName("Test Survey");
        survey.setDescription("Test Description");
        survey.setCategory("Food");
    }

    //create a demo survey
    private Survey addTestSurvey() throws Exception {
        Survey survey = new Survey();
        survey.setName("Test Survey");
        survey.setDescription("Test Description");
        survey.setCategory("Food");
        return service.postSurvey(survey);
    }

    @Before
    public void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Test
    public void getSurveys() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                when().
                get("/api/surveys").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void controller_post_survey_success_if_survey_returned_in_body() {
        //send the request to the controller method
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                body(survey).
                when().
                post("/api/survey").
                then().
                assertThat().
                statusCode(200).
                body("name", equalTo("Test Survey"));
    }

    @Test
    public void testGetSurveyDetails() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .contentType("application/json")
                .when()
                .get("/api/survey/" + survey.getId())
                .then()
                .assertThat()
                .statusCode(200)
                .body("id", equalTo(survey.getId()));
    }

    @Test
    public void testPostAddSurvey() {
        Survey s = new Survey();
        s.setName("This is a test survey");
        s.setDescription("This is test description");
        s.setCategory("Test category");

        Header header = new Header("token", "mpptoken");
        given().
                header(header).contentType("application/json").body(s).when().post("/api/survey").then().assertThat().statusCode(200).body("active", equalTo(true));
    }

    @Test
    public void testEditSurveyDetails() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .contentType("application/json")
                .body(survey)
                .when()
                .post("/api/survey/edit")
                .then()
                .assertThat()
                .statusCode(200)
                .body("id", equalTo(survey.getId()));
    }

    @Test
    public void testDeleteSurvey() throws Exception {
        Survey s = this.addTestSurvey();
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .body(s)
                .when()
                .get("/api/survey/delete")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testActivateSurvey() throws Exception {
        Survey s = this.addTestSurvey();
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .when()
                .get("/api/survey/activate" + s.getId())
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testDeActivateSurvey() throws Exception {
        Survey s = this.addTestSurvey();
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .when()
                .get("/api/survey/deactivate" + s.getId())
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testGetFrontendSurveys() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .contentType("application/json")
                .get("/api/user/surveys")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testGetSurveyByCategory() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .contentType("application/json")
                .get("/api/surveys/" + survey.getCategory())
                .then()
                .statusCode(200);
    }

    @Test
    public void testGetSearchSurveyByCategory() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header)
                .contentType("application/json")
                .get("/api/surveys/search/" + survey.getCategory())
                .then()
                .statusCode(200);
    }

}
