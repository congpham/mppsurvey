package com.app.MasterSurvey.controller;

import static io.restassured.RestAssured.given;


import com.app.MasterSurvey.service.QuestionService;
import io.restassured.http.Header;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Question;

import io.restassured.RestAssured;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MasterSurveyApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class QuestionControllerTest {
    @LocalServerPort
    private int port;
    private Question question1;

    @Autowired
    private QuestionService service;

    public QuestionControllerTest() {
        this.question1 = new Question();
        question1.setId("1");
        question1.setContent("MC Question");
        String[] options = new String[]{"a", "b", "c", "d"};
        question1.setOptions(options);
        question1.setDefaultOption("a");
        question1.setType("mc");
    }

    @Before
    public void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Test
    public void controller_post_survey_success_if_survey_returned_in_body() {
        //send the request to the controller method

    }

    @Test
    public void testGetSurveyQuestions() {
        //send the request to the controller method
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                when().
                get("/api/survey/1/question").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void testGetSurveyQuestion() {
        //send the request to the controller method
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                when().
                get("/api/survey/1/question/" + question1.getId()).
                then().
                assertThat().
                statusCode(200).
                body("id", equalTo(question1.getId()));
    }

    @Test
    public void testPostAddSurveyQuestionS() {
        List<Question> questions = new ArrayList<Question>();
        String[] options = {"1", "2", "3", "4"};

        Question question2 = new Question();
        question2.setContent("This is content 1");
        question2.setOptions(options);
        question2.setType("mc");
        question2.setDefaultOption("3");

        Question question3 = new Question();
        question3.setContent("This is content 2");
        question3.setType("oe");

        questions.add(question2);
        questions.add(question3);

        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                body(questions).
                when().
                post("/api/survey/1/questions/").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void testPostAddSurveyQuestion() {
        String[] options = {"1", "2", "3", "4"};
        Question question2 = new Question();
        question2.setContent("This is content 1");
        question2.setOptions(options);
        question2.setType("mc");
        question2.setDefaultOption("3");

        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                body(question2).
                when().
                post("/api/survey/1/question/").
                then().
                assertThat().
                statusCode(200).
                assertThat().
                body("content", equalTo(question2.getContent()));
    }

    //should return null
    //because the question does not exist is db
    @Test
    public void testPostEditSurveyQuestion() {
        String[] options = {"1", "2", "3", "4"};
        Question question2 = new Question();
        question2.setContent("This is content 1");
        question2.setOptions(options);
        question2.setType("mc");
        question2.setDefaultOption("3");
        question2.setId("2");

        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                body(question2).
                when().
                post("/api/survey/1/question/edit").
                then().
                assertThat().
                statusCode(200);
    }

    //should return null
    //because the question does not exist is db
    @Test
    public void testPostDeleteSurveyQuestion() {
        String[] options = {"1", "2", "3", "4"};
        Question question2 = new Question();
        question2.setContent("This is content 1");
        question2.setOptions(options);
        question2.setType("mc");
        question2.setDefaultOption("3");
        question2.setId("2");

        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                body(question2).
                when().
                post("/api/survey/1/question/delete").
                then().
                assertThat().
                statusCode(200);
    }
}
