package com.app.MasterSurvey.controller;

import com.app.MasterSurvey.domain.Category;
import com.app.MasterSurvey.service.CategoryService;
import io.restassured.http.Header;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Survey;

import java.util.Random;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MasterSurveyApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)

public class CategoryControllerTest {
    @LocalServerPort
    private int port;
    private Survey survey;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void testGetAllCategories() {
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                when().
                get("/api/category").
                then().
                assertThat().
                statusCode(200);
    }

    private Category createDemoCategory(String name) throws Exception {
        Category category = new Category(name, (short) 1);
        return categoryService.addCategory(category);
    }

    protected String getRandomName() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    @Test
    public void testPostAddCategory() {
        String name = this.getRandomName();
        Category category = new Category(name, (short) 1);
        System.out.println("Cat id " + category.getId());
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json")
                .body(category).
                when().
                post("/api/category").
                then().
                assertThat().
                statusCode(200).
                body("name", equalTo(name));
    }

    @Test
    public void testPostEditCategory() throws Exception {
        String name = this.getRandomName();
        Category category = this.createDemoCategory(name);
        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json")
                .body(category).
                when().
                post("/api/category").
                then().
                assertThat().
                statusCode(200)
                .body("id", equalTo(category.getId()));
    }

    @Test
    public void testGetCategory() throws Exception {
        String name = this.getRandomName();
        Category category = createDemoCategory(name);

        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                when().
                get("/api/category/" + category.getId()).
                then().
                assertThat().
                statusCode(200)
                .body("id", equalTo(category.getId()));
    }

    @Test
    public void testGetDeleteCategory() throws Exception {
        String name = this.getRandomName();
        Category category = createDemoCategory(name);

        Header header = new Header("token", "mpptoken");
        given().
                header(header).
                contentType("application/json").
                when().
                get("/api/category/" + category.getId()).
                then().
                assertThat().
                statusCode(200)
                .body("id", equalTo(category.getId()));
    }
}
