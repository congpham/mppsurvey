package com.app.MasterSurvey.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Question;
import com.app.MasterSurvey.domain.Result;
import com.app.MasterSurvey.domain.Survey;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = MasterSurveyApplication.class
)
public class ResultServiceTest {
	private Survey survey;
	private Question question1;
	private Question question2;
	private Result result1;
	private Result result2;
	private List<Result> results = new ArrayList<Result>();
	@Autowired
	private ResultService resultService;
	@Autowired
	private SurveyService surveyService;
	
	public ResultServiceTest () {
		this.survey = new Survey();
		survey.setId("1");
		survey.setName("Test Survey");
		survey.setDescription("Test Description");
		survey.setCategory("Food");
		
		this.question1 = new Question();
		question1.setId("1");
		question1.setContent("MC Question");
		String[] options = {"a", "b", "c", "d"};
		question1.setOptions(options);
		question1.setDefaultOption("a");
		question1.setType("mc");
		
		this.question2 = new Question();
		question2.setId("2");
		question2.setContent("OE Question");
		question2.setType("oe");
		
		survey.addQuestion(question1);
		survey.addQuestion(question2);
		
		result1 = new Result();
		result1.setQuestionId(question1.getId());
		result1.setSurveyId(survey.getId());
		result1.setAnswer("a");
		result1.setRating(3);
		
		result2 = new Result();
		result2.setQuestionId(question2.getId());
		result2.setSurveyId(survey.getId());
		result2.setAnswer("Result 2");
		result2.setRating(4);
		
		results.add(result1);
		results.add(result2);
	}
	@Test
	public void test_result_service_success_if_found() throws Exception {
		surveyService.postSurvey(survey);
		//test add all
		resultService.addResults(results);
		//test add
		resultService.addResult(result1);
		//test get all
		assert(resultService.fetchAll().size() > 0);
		//test get by question id
		assert(resultService.fetchAllByQuestionId(survey.getId(), question1.getId()).size() > 0);
	}
	@Test
	public void test_building_chart() throws Exception {
		//test building chart
		assert(resultService.fetchSurveyChart(survey.getId()).getSurveyId() != null);
	}
}
