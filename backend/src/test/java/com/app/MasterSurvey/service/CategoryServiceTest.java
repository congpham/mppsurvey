package com.app.MasterSurvey.service;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MasterSurveyApplication.class)
public class CategoryServiceTest {
    @Autowired
    private CategoryService categoryService;


    private Category createDemoCategory(String name) throws Exception {
        Category category = new Category(name, (short) 1);
        return categoryService.addCategory(category);
    }

    protected String getRandomName() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    @Test
    public void testGetAllCategories() {
        List<Category> categories = categoryService.getAll();
        assert (categories == null || categories.size() > 0);
    }

    @Test
    public void testFindById() throws Exception {
        Category category = this.createDemoCategory(getRandomName());
        Category category1 = categoryService.findById(category.getId());
        assert (category1 != null && category1.getId().equals(category.getId()));
    }

    @Test
    public void testAddCategory() throws Exception {
        String name = this.getRandomName();
        Category category = new Category(name, (short) 1);
        Category category1 = categoryService.addCategory(category);
        assert (category1 != null && category1.getName().equals(name));
    }

    @Test
    public void testEditCategory() throws Exception {
        Category category = this.createDemoCategory(getRandomName());

        String name = this.getRandomName();
        category.setName(name);

        Category category1 = categoryService.updateCategory(category);
        assert (category1 != null && category1.getName().equals(name));
    }

    @Test
    public void testDeleteCategory() throws Exception {
        Category category = this.createDemoCategory(getRandomName());

        Category category1 = categoryService.deleteCategoryById(category.getId());
        assert (category1 != null && category1.getId().equals(category.getId()));
    }
}
