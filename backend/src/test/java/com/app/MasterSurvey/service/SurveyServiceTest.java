package com.app.MasterSurvey.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Survey;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MasterSurveyApplication.class)
public class SurveyServiceTest {
    private Survey survey;

    @Autowired
    private SurveyService surveyService;

    public SurveyServiceTest() {
        this.survey = new Survey();
        survey.setId("1");
        survey.setName("Test Survey");
        survey.setDescription("Test Description");
        survey.setCategory("Food");
    }

    private Survey addTestSurvey() throws Exception {
        Survey survey = new Survey();
        survey.setName("Test Survey");
        survey.setDescription("Test Description");
        survey.setCategory("Food");
        return surveyService.postSurvey(survey);
    }

    @Test
    public void testSurveyNotFound() {
        Survey s = surveyService.findById(survey.getId());
        assert (s == null);
    }

    @Test
    public void testSurveyExists() throws Exception {
        Survey testSurvey = this.addTestSurvey();
        Survey s = surveyService.findById(testSurvey.getId());
        assert (s.getId() != null);
    }

    @Test
    public void testGetAllSurveys() {
        assert (surveyService.getSurveys().size() > 0);
    }

    @Test
    public void testSearchSurveysByCategory() throws Exception {
        String cat = "Food";
        String cat1 = "foo";
        String cat2 = "f";
        String cat3 = "ood";
        String cat4 = null;
        String cat5 = "";

        assert (surveyService.searchSurveysByCategory(cat).size() > 0);
        assert (surveyService.searchSurveysByCategory(cat1).size() > 0);
        assert (surveyService.searchSurveysByCategory(cat2).size() > 0);
        assert (surveyService.searchSurveysByCategory(cat3).size() > 0);
        assert (surveyService.searchSurveysByCategory(cat4).size() > 0);
        assert (surveyService.searchSurveysByCategory(cat5).size() > 0);
    }

    @Test
    public void delete_survey_success_if_active_is_false() throws Exception {
        surveyService.deleteSurvey(survey);
        assert (surveyService.findById(survey.getId()) == null);
    }

    @Test
    public void active_survey_success_if_active_is_true() throws Exception {
        Survey testSurvey = this.addTestSurvey();
        assert (surveyService.activateSurvey(testSurvey.getId()).isActive());
    }
}
