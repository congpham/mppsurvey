package com.app.MasterSurvey.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.builder.ToStringExclude;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.MasterSurvey.MasterSurveyApplication;
import com.app.MasterSurvey.domain.Question;
import com.app.MasterSurvey.domain.Survey;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MasterSurveyApplication.class
)
public class QuestionServiceTest {
    private Survey survey;
    private Question question1;
    private Question question2;

    List<Question> questions = new ArrayList<Question>();

    @Autowired
    private QuestionService questionService;

    @Autowired
    private SurveyService service;

    public QuestionServiceTest() {
        this.survey = new Survey();
        survey.setId("1");
        survey.setName("Test Survey");
        survey.setDescription("Test Description");
        survey.setCategory("Food");

        this.question1 = new Question();
        //question1.setId("1");
        question1.setContent("MC Question");
        String[] options = new String[]{"a", "b", "c", "d"};
        question1.setOptions(options);
        question1.setDefaultOption("a");
        question1.setType("mc");

        this.question2 = new Question();
        //question2.setId("2");
        question2.setContent("OE Question");
        question2.setType("oe");

        questions.add(question1);
        questions.add(question2);

        survey.setQuestions(questions);
    }

    private Survey createDemoSurvey() throws Exception {
        Survey survey = new Survey();
        survey.setName("Test Survey");
        survey.setDescription("Test Description");
        survey.setCategory("Food");
        Survey addedSurvey = service.postSurvey(survey);
        return questionService.addSurveyQuestions(addedSurvey.getId(), questions);
    }

    @Test
    public void testEditQuestion() throws Exception {
        Survey survey = this.createDemoSurvey();
        Random random = new Random();
        int randomIndex = random.nextInt(survey.getQuestions().size());
        Question randomQuestion = survey.getQuestions().get(randomIndex);
        Survey edited = questionService.editQuestion(randomQuestion, survey.getId());
        assert (edited != null && edited.getId().equals(survey.getId()));
    }

    @Test
    public void testDeleteQuestion() throws Exception {
        Survey survey = this.createDemoSurvey();
        Random random = new Random();
        int randomIndex = random.nextInt(survey.getQuestions().size());
        Question randomQuestion = survey.getQuestions().get(randomIndex);
        Survey deleted = questionService.deleteQuestion(randomQuestion, survey.getId());
        assert (deleted != null && deleted.getId().equals(survey.getId()));
    }

    @Test
    public void staticMethodTestCheckIfExists() throws Exception {
        Survey survey = this.createDemoSurvey();
        Random random = new Random();
        int randomIndex = random.nextInt(survey.getQuestions().size());
        Question randomQuestion = survey.getQuestions().get(randomIndex);
        assert (QuestionService.isExisted(survey, randomQuestion));
    }

    @Test
    public void testGetSurveyQuestions() throws Exception {
        Survey survey = this.createDemoSurvey();
        List<Question> questions = questionService.getSurveyQuestions(survey.getId());
        assert (questions.size() == 2);
    }

    @Test
    public void testGetSurveyQuestion() throws Exception {
        Survey survey = this.createDemoSurvey();
        Random random = new Random();
        int randomIndex = random.nextInt(survey.getQuestions().size());
        Question randomQuestion = survey.getQuestions().get(randomIndex);
        Question question = questionService.getSurveyQuestion(survey.getId(), randomQuestion.getId());

        assert (question != null && question.getId().equals(randomQuestion.getId()));
    }

    @Test
    public void testAddSurveyQuestion() throws Exception {
        Survey survey = this.createDemoSurvey();
        Question question = new Question();
        question.setContent("Question 3");
        question.setType("oe");
        Question question3 = questionService.addSurveyQuestion(survey.getId(), question);
        assert (question3 != null && question3.getId() != null);
    }

    @Test
    public void testAddSurveyQuestions() throws Exception {
        Survey survey = this.createDemoSurvey();
        List<Question> questions = new ArrayList<Question>();
        Question question = new Question();
        question.setContent("Question 3");
        question.setType("oe");
        questions.add(question);
        Survey s = questionService.addSurveyQuestions(survey.getId(), questions);

        assert (s != null && s.getQuestions().size() == 3);
    }
}
